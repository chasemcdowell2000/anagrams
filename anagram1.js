const button = document.getElementById("findButton");
const arr = [];

function getAnagramsOf(a) {
    let alphaText = a.toLowerCase().split("").sort().join("").trim();

    for (let i = 0; i < words.length; i++) {
        let sorted = words[i].toLowerCase().split('').sort().join('').trim();
        if (alphaText === sorted) {
            arr.push(words[i]);
        }
    }
    return arr;
}


button.onclick = function () {
    let typedText = document.getElementById("input").value;
    getAnagramsOf(typedText);
    let div = document.createElement("div")
    let anagrams = document.createTextNode(arr);
    let destination = document.getElementById("anagram");
    div.appendChild(anagrams);
    destination.appendChild(div);
}